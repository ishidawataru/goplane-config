scenario3
===
- two switches, two (physical) hosts connected to each switch
- two virtual network(tenant) which contains two (virtual) hosts
- tagged (vid 10, 20)

# topology
```
--------swp1    --------
|  s1  |--------|  s2  |
--------    swp2--------
   |swp9           |swp9
   |               |
   |eth4           |eth5
--------        --------
|  h1  |        |  h2  |
-------         --------
```
