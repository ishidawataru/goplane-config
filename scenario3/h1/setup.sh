#!/bin/bash

ip li add link eth4 name eth4.10 type vlan id 10
ip li set up dev eth4.10

ip li add link eth4 name eth4.20 type vlan id 20
ip li set up dev eth4.20

ip a add 192.168.10.2/24 dev eth4.10
ip a add 192.168.20.2/24 dev eth4.20
