goplane-config
===

# scenario1
- two switches, two hosts connected to each switch
- one virtual network(tenant)
- untagged

# scenario2
- two switches, two hosts connected to each switch
- one virtual network(tenant)
- tagged (vid 10)

# scenario3
- two switches, two hosts connected to each switch
- two virtual network(tenant)
- tagged (vid 10, 20)

# scenario4
- this scenario shows how to contain multiple member interface to one virtual network
- two switches, three hosts connected to each switch
- two virtual network(tenant)
- tagged (vid 10, 20)
