#!/bin/bash

# TODO initialize function

#ip tuntap del dev tap0 mode tun
ip li set down swp1
ip li set down swp2
ip li set down swp9.0
ip li set down swp9
ip li set down vtep10
ip li set down br02
ip li del br02

#ip tuntap add dev tap0 mode tun
#ip li set up tap0
#ip a add 192.168.0.1/32 dev tap0

ip li set up swp1
ip a add 192.168.1.1/24 dev swp1

ip li set up swp2
ip a add 192.168.3.2/24 dev swp2
#
#ip r add 192.168.0.2/32 via 192.168.1.2
#ip r add 192.168.0.3/32 via 192.168.3.1

ip link set up dev swp9
ip link add link swp9 name swp9.0 type vlan id 0
ip link set up dev swp9.0

#ip link add vtep10 type vxlan id 10 nolearning local 192.168.0.1
#ip link set up dev vtep10
#
#ip link add br01 type bridge
#ip link set up dev br01
#
#ip link set master br01 dev swp9.0
#ip link set master br01 dev vtep10

#bridge fdb add 22:22:22:22:22:33 dev vtep10 dst 192.168.0.2
#bridge fdb add 22:22:22:22:22:44 dev vtep10 dst 192.168.0.3

ip li set down swp5
ip li set down swp6
iptables -F

ip li set up swp5
ip li set up swp6

cl-acltool -i -p span.rules

ip li add br02 type bridge
ip li set up dev br02

ip li set master br02 dev swp6

ip r del 192.168.0.2
ip r add 192.168.0.2 via 192.168.1.2 src 192.168.0.1
ip r del 192.168.0.3
ip r add 192.168.0.3 via 192.168.3.1 src 192.168.0.1

echo "==interface settings=="
ifconfig

echo "==bridge settings=="
brctl show

echo "==fdb settings=="
bridge fdb show

echo "==span settings=="
iptables -L -v

