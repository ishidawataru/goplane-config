scenario1
===
- two switches, two hosts connected to each switch
- one virtual network(tenant)
- untagged

# topology
```
--------swp1    --------
|  s1  |--------|  s2  |
--------    swp2--------
   |swp9           |swp9
   |               |
   |eth4           |eth5
--------        --------
|  h1  |        |  h2  |
-------         --------
```
