#!/bin/bash

# TODO initialize function

ip li set up swp2
ip a add 192.168.1.2/24 dev swp2
#
ip link set up dev swp9
ip link add link swp9 name swp9.0 type vlan id 0
ip link set up dev swp9.0

ip li set down swp5
ip li set down swp6
iptables -F

ip li set up swp5
ip li set up swp6
cl-acltool -i -p span.rules

ip li add br02 type bridge
ip li set up dev br02

ip li set master br02 dev swp6

ip r del 192.168.0.1
ip r add 192.168.0.1 via 192.168.1.1 src 192.168.0.2
