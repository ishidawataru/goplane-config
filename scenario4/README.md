scenario4
===
- this scenario shows how to contain multiple member interface to one virtual network
- two switches, three hosts connected to each switch
- two virtual network(tenant)
- tagged (vid 10, 20)

# topology
```
--------swp1    --------swp10
|  s1  |--------|  s2  |-------
--------    swp2--------      |
   |swp9           |swp9      |
   |               |          |
   |eth4           |eth5      | eth4
--------        --------   -------
|  h1  |        |  h2  |   |  h3  |
-------         --------   -------
```
