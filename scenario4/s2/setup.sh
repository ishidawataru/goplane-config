#!/bin/bash

ip li set up swp2
ip a add 192.168.1.2/24 dev swp2

ip link set up dev swp9
ip link add link swp9 name swp9.10 type vlan id 10
ip link set up dev swp9.10
ip link add link swp9 name swp9.20 type vlan id 20
ip link set up dev swp9.20

ip link set up dev swp10
ip link add link swp10 name swp10.10 type vlan id 10
ip link set up dev swp10.10
ip link add link swp10 name swp10.20 type vlan id 20
ip link set up dev swp10.20

ip li set down swp5
ip li set down swp6
iptables -F

ip li set up swp5
ip li set up swp6

ip link add link swp6 name swp6.10 type vlan id 10
ip link add link swp6 name swp6.20 type vlan id 20
ip link set up dev swp6.10
ip link set up dev swp6.20

cl-acltool -i -p span.rules

ip li add s_br10 type bridge
ip li set up dev s_br10
ip li set master s_br10 dev swp6.10

ip li add s_br20 type bridge
ip li set up dev s_br20
ip li set master s_br20 dev swp6.20

ip r del 192.168.0.1
ip r add 192.168.0.1 via 192.168.1.1 src 192.168.0.2
