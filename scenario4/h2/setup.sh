#!/bin/bash

ip li add link eth5 name eth5.10 type vlan id 10
ip li set up dev eth5.10

ip li add link eth5 name eth5.20 type vlan id 20
ip li set up dev eth5.20

ip a add 192.168.10.3/24 dev eth5.10
ip a add 192.168.20.3/24 dev eth5.20
